package LS_1;

import java.util.ArrayList;

public class SwitchValues {
    public void switchWithAdditionalVariable(Integer a, Integer b) {
        Integer c = a;

        System.out.println("before switch a= " + a + " b= " + b);
        a = b;
        b = c;
        System.out.println("after switch a= " + a + " b= " + b);
    }

    public void switchWithoutAdditionalVariable(Integer a, Integer b) {
        System.out.println("before switch a= " + a + " b= " + b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("after switch a= " + a + " b= " + b);
    }
}
