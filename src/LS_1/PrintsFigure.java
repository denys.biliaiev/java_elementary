package LS_1;

public class PrintsFigure {
    public void printString() {
        String simbolsFigure = "";
        String [] [] figures = {
            {"#######", "######", "######"},
            {"#     #", "    # ", " #    "},
            {"#     #", "   #  ", "  #   "},
            {"#     #", "  #   ", "   #  "},
            {"#     #", " #    ", "    # "},
            {"#######", "######", "######"},
        };

        for (Integer row = 0; row < figures.length; row++) {
            if (row > 0) {
                simbolsFigure += "\r\n";
            }
            for (Integer coll = 0; coll < figures[0].length; coll++) {
                simbolsFigure += figures[row][coll];
                simbolsFigure += "       ";
            }
        }

        System.out.println(simbolsFigure);
    }
}
