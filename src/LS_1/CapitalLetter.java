package LS_1;

public class CapitalLetter {
    public void firstLetterToCapital(String str) {
        String words[] = str.split("\\s");
        String capitalisedStr = "";

        for (String word:words) {
            String  capitalisedWord = word.substring(0, 1).toUpperCase() + word.substring(1);

            capitalisedStr += capitalisedWord + " ";
        }

        System.out.println(capitalisedStr);
    }
}