package LS_1;

public class Main {
    public static void main(String[] args) {
        System.out.println("-----------1. Switch Values-------------");
        int a = 2;
        int b = 5;

        SwitchValues switchValues = new SwitchValues();

        switchValues.switchWithAdditionalVariable(a, b);
        switchValues.switchWithoutAdditionalVariable(a, b);

        System.out.println("-----------2. Animals Year-------------");
        AnimalsYear animalsYear = new AnimalsYear(5);


        System.out.println("-----------3. Prints Figure-------------");
        PrintsFigure printsFigure = new PrintsFigure();

        printsFigure.printString();

        System.out.println("-----------2. Capitalise Letter-------------");

        CapitalLetter capitalLetter = new CapitalLetter();

        capitalLetter.firstLetterToCapital("this is just an example string for test");
    }
}
