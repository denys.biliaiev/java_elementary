package LS_1;

public class AnimalsYear {
    private Integer years = 1;

    public AnimalsYear(Integer years) {
        this.years = (Integer)years;

        System.out.println("HumanYears: " + years);
        System.out.println("CatYears: " + calculateYears(Animal.CAT));
        System.out.println("DogYears: " + calculateYears(Animal.DOG));
    }

    public Integer calculateYears(Animal animal) {
        Integer firstYear = 15;
        Integer secondYear = 9;
        Integer nextYear = 4;

        if (animal == Animal.DOG) {
            nextYear = 6;
        }

        if (years == 2 ) {
            return firstYear + secondYear;
        }

        if (years > 2 ) {
            return firstYear + secondYear + (years - 2) * nextYear;
        }

        return firstYear;
    }
}
